<?php // Template Name: Videos Interna ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-videos-interna">

        <!-- UOL WRAPPER -->
        <?php require 'templates/uol-wrapper.php' ?>

        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- NAVEGACAO -->
        <?php require 'templates/navegacao.php' ?>

        <!-- VIDEOS 1 WRAPPER -->
        <div class="videos1-wrapper">
            <div class="header">
                <h3 class="titulo">Vídeos</h3>
            </div>
            <div class="itens">
                <div class="item">
                    <div class="wrapper">
                        <figure>
                          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video1.jpg" alt="" title="">  
                        </figure>
                        <div class="info">
                            <p class="area">Cuidados</p>
                            <h5 class="titulo">Dengue é uma doença grave</h5>
                        </div>
                    </div>
                    <div class="new-wrapper">
                        <div class="header">
                            <h4 class="titulo">Autismo e formas de brincar | Nuno Lobo Antunes</h4>
                        </div>
                        <div class="body">
                            <p class="texto">A forma de brincar de uma criança autista pode ser bem diferente daquela das não-autistas. O Dr. Nuno Lobo Antunes fala sobre o tema neste vídeo.</p>
                        </div>
                    </div>
                    <div class="compartilhamento-wrapper">
                        <div class="info">
                            <span class="num-compartilhamentos">20</span>
                            <p class="texto">compartilhamentos</p>
                        </div>
                        <div class="actions">
                            <a href="#" class="botao">Compartilhar</a>
                            <div class="redes-sociais">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/twitter-rodape.png" alt="Twitter">
                                </figure>
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram-rodape.png" alt="Instagram">
                                </figure>
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook-rodape.png" alt="Facebook">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="destaque-lateral">
                        <h3 class="titulo">Vídeos Relacionados</h3>
                        <div class="itens-p">
                            <a href="#">
                                <article class="item">
                                    <figure>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                    </figure>
                                    <div class="info">
                                        <h5 class="titulo">Saúde intima</h5>
                                        <p class="texto">Como prevenir a vaginose bacteriana</p>
                                    </div>
                                </article>
                            </a>
                            <a href="#">
                                <article class="item">
                                    <figure>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                    </figure>
                                    <div class="info">
                                        <h5 class="titulo">Saúde intima</h5>
                                        <p class="texto">Como prevenir a vaginose bacteriana</p>
                                    </div>
                                </article>
                            </a>
                            <a href="#">
                                <article class="item">
                                    <figure>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                    </figure>
                                    <div class="info">
                                        <h5 class="titulo">Saúde intima</h5>
                                        <p class="texto">Como prevenir a vaginose bacteriana</p>
                                    </div>
                                </article>
                            </a>
                            <a href="#">
                                <article class="item">
                                    <figure>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                    </figure>
                                    <div class="info">
                                        <h5 class="titulo">Saúde intima</h5>
                                        <p class="texto">Como prevenir a vaginose bacteriana</p>
                                    </div>
                                </article>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- TYPE 7 WRAPPER -->
        <div class="type7-wrapper">
            <div class="header">
                <h3 class="titulo">Vídeos Novos</h3>
                <select name="" id="" class="form-select">
                    <option selected>Categorias</option>
                    <option value="Categoria 1">Categoria 1</option>
                    <option value="Categoria 2">Categoria 2</option>
                    <option value="Categoria 3">Categoria 3</option>
                </select>
            </div>
        </div>

        <!-- TYPE 5 WRAPPER -->
        <div class="type5-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="area-botao">
                <a href="#" class="botao botao-principal">Ver Mais</a>
            </div>
        </div>

    </div>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>