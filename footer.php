	<!-- RODAPE -->
    <footer class="rodape">
        <div class="header">
            <a href="home">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/logo-rodape.png" alt="Portal Drauzio" class="logo">
            </a>
            <div class="redes-sociais">
                <a href="#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/twitter-rodape.png" alt="Twitter">
                </a>
                <a href="#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram-rodape.png" alt="Instagram">
                </a>
                <a href="#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook-rodape.png" alt="Facebook">
                </a>
            </div>
        </div>
        <div class="busca">
            <label for="busca">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/busca.png" alt="Busca">
            </label>
            <input type="text" id="busca" placeholder="PESQUISAR">
        </div>
        <!-- NAVEGACAO -->
        <nav class="navegacao">
            <div class="links">
                <a href="#" class="link">Coronavírus</a>
                <a href="#" class="link">Drauzio</a>
                <a href="#" class="link">Doenças e sintomas</a>
                <a href="#" class="link">Ambulatório</a>
                <a href="#" class="link">Sexualidade</a>
                <a href="#" class="link">Criança</a>
                <a href="#" class="link">Mulher</a>
                <a href="#" class="link">Idoso</a>
                <a href="#" class="link">Checagens</a>
                <a href="#" class="link">Podcasts</a>
                <a href="#" class="link">Vídeos</a>
            </div>
        </nav>
        <div class="copy">
            <div class="info">
                <a href="#">Contato</a>
                <a href="#">Termos de uso</a>
            </div>
            <div class="assinatura">
                <a href="#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/logo-uzmk.png" alt="UZMK">
                </a>
            </div>
        </div>
    </footer>

    <!-- JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/busca.js"></script>

    <!-- SPLIDE -->
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/carousels.js"></script>

</body>

</html>