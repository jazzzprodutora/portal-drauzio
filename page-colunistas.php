<?php // Template Name: Colunistas ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-colunistas">

        <!-- UOL WRAPPER -->
        <?php require 'templates/uol-wrapper.php' ?>

        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- NAVEGACAO -->
        <?php require 'templates/navegacao.php' ?>

        <!-- TYPE 8 WRAPPER -->
        <div class="type8-wrapper">
            <div class="header">
                <h3 class="titulo">Colunistas</h3>
            </div>
            <div class="itens">
                <div class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/perfil.png" alt="Maiara Ribeiro" title="">
                    </figure>
                    <div class="info">
                        <h5 class="titulo">Maiara Ribeiro</h5>
                        <p class="texto">Maiara Ribeiro é jornalista, repórter do Portal Drauzio Varella e interessada em temas relacionados a saúde da mulher e deficiências na infância.</p>
                    </div>
                </div>
                <div class="mais-item">
                    <h5 class="titulo">+ Colunistas</h5>
                    <select name="" id="" class="form-select">
                        <option selected>Categorias</option>
                        <option value="Categoria 1">Categoria 1</option>
                        <option value="Categoria 2">Categoria 2</option>
                        <option value="Categoria 3">Categoria 3</option>
                    </select>
                </div>
            </div>
        </div>

        <!-- TYPE 7 WRAPPER -->
        <div class="type7-wrapper">
            <div class="header">
                <h3 class="titulo">Matérias</h3>
                <select name="" id="" class="form-select">
                    <option selected>Categorias</option>
                    <option value="Categoria 1">Categoria 1</option>
                    <option value="Categoria 2">Categoria 2</option>
                    <option value="Categoria 3">Categoria 3</option>
                </select>
            </div>
        </div>

        <!-- TYPE 5 WRAPPER -->
        <div class="type5-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="area-botao">
                <a href="#" class="botao botao-principal">Ver Mais</a>
            </div>
        </div>
    </div>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>