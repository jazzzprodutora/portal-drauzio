const areaClique = document.querySelector('#area-clique-busca');
const busca = document.querySelector('.busca');
const buscaAberto = document.querySelector('.busca-aberto');
const fecharBusca = document.querySelector('.fechar-busca');
areaClique.addEventListener('click', () => {
  buscaAberto.classList.toggle('busca-aberto-efeito');
});
fecharBusca.addEventListener('click', () => {
  buscaAberto.classList.remove('busca-aberto-efeito');
});
