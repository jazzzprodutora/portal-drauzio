// CAROUSEL TYPE 9
document.addEventListener('DOMContentLoaded', function () {
  new Splide('#carousel-type9', {
    type: 'loop',
    perPage: 3,
    gap: '2vw',
    autoplay: 'true',
    speed: '400',
    keyboard: 'true',
    updateOnMove: 'true',
    pagination: 'false',
    breakpoints: {
      600: {
        perPage: 1,
      },
    },
  }).mount();
});

// CAROUSEL TYPE 10
document.addEventListener('DOMContentLoaded', function () {
  new Splide('#carousel-type10', {
    type: 'loop',
    perPage: 2,
    gap: '2vw',
    autoplay: 'true',
    speed: '400',
    keyboard: 'false',
    updateOnMove: 'true',
    pagination: 'false',
    breakpoints: {
      600: {
        perPage: 1,
      },
    },
  }).mount();
});

// CAROUSEL VIDEOS 3
document.addEventListener('DOMContentLoaded', function () {
  new Splide('#carousel-videos3', {
    type: 'loop',
    perPage: 1,
    gap: '0vw',
    autoplay: 'true',
    speed: '400',
    keyboard: 'false',
    updateOnMove: 'true',
    pagination: 'false',
    breakpoints: {
      600: {
        perPage: 1,
      },
    },
  }).mount();
});
