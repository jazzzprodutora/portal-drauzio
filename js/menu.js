const menu = document.querySelector('.menu-compacto');
const linha1 = document.querySelector('.menu-compacto .linha1');
const linha2 = document.querySelector('.menu-compacto .linha2');
const linha3 = document.querySelector('.menu-compacto .linha3');
const nomeMenu = document.querySelector('.menu-compacto span');
const menuAberto = document.querySelector('.menu-aberto');
menu.addEventListener('click', () => {
  nomeMenu.classList.toggle('visivel');
  linha1.classList.toggle('linha1-efeito');
  linha2.classList.toggle('linha2-efeito');
  linha3.classList.toggle('linha3-efeito');
  menuAberto.classList.toggle('menu-aberto-efeito');
});
