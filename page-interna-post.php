<?php // Template Name: Interna Post ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <!-- UOL WRAPPER -->
    <?php require 'templates/uol-wrapper.php' ?>

    <!-- CABECALHO -->
    <?php require 'templates/cabecalho.php' ?>

    <!-- NAVEGACAO -->
    <?php require 'templates/navegacao.php' ?>

    <div class="page-interna-post">
        <!-- CONTEUDO -->
        <div class="content-wrapper">
            <!-- LEFT WRAPPER -->
            <div class="left-wrapper">
                <div class="titulo-header">
                    <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                </div>
                <div class="texto-header">
                    <p class="texto">Aquela agitação diante de certas circunstâncias é uma reação natural, mas em determinado nível pode configurar um transtorno. Veja os principais sinais de ansiedade para saber se é hora de procurar um médico.</p>
                </div>
                <div class="autor">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/perfil.png" alt="Maiara Ribeiro" title="">
                    </figure>
                    <p class="nome">Maiara Ribeiro postou em <span class="area">Psiquiatria</span></p>
                </div>
                <!-- VIDEOS 1 WRAPPER -->
                <div class="videos1-wrapper">
                    <div class="itens">
                        <div class="wrapper">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img8.jpg" alt="" title="">
                            </figure>
                        </div>
                    </div>
                    <div class="compartilhamento-wrapper">
                        <div class="info">
                            <span class="num-compartilhamentos">20</span>
                            <p class="texto">compartilhamentos</p>
                        </div>
                        <div class="actions">
                            <a href="#" class="botao">Compartilhar</a>
                            <div class="redes-sociais">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/twitter.png" alt="Twitter">
                                </figure>
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram.png" alt="Instagram">
                                </figure>
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.png" alt="Facebook">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- conteudo texto -->
                <p class="texto-conteudo">
                    É normal nos sentirmos ansiosos diante de ocasiões que geram expectativa, medo ou indecisão. Situações como uma entrevista de emprego, uma viagem sonhada ou o diagnóstico de uma doença, por exemplo, causam ansiedade e não há nada de estranho nisso. O problema é quando a preocupação é tão excessiva que causa prejuízo direto e por um período prolongado. Nesses casos, o mais indicado é buscar a ajuda de um médico psiquiatra, responsável por diagnosticar o problema e indicar o tratamento mais adequado para cada caso.
                    <br><br>
                    Veja abaixo 5 sintomas de que a sua ansiedade precisa de uma atenção. Todavia, esses sintomas não implicam imediatamente no diagnóstico de ansiedade. É necessário procurar um médico e relatar o caso, pois só ele poderá analisar adequadamente.
                    <br><br><br>

                    1. NÃO CONSEGUIR DORMIR DIREITO<br><br>
                    A alteração do sono é um dos principais sintomas do transtorno de ansiedade generalizada (TAG). É comum que a pessoa ansiosa sofra de insônia porque ela não se desconecta das suas preocupações nem na hora de dormir. Se noites seguidas você não consegue adormecer porque fica pensando em problemas, ou até dorme porque está exausto, mas desperta no meio da noite e logo é invadido pelos mesmos pensamentos, você pode estar muito ansioso.
                    <br><br>
                    Veja também: Por que estamos tão ansiosos?

                    <br><br><br>
                    2. CANSAÇO CONSTANTE<br><br>
                    A ansiedade constante pode levar à exaustão mental, mas também à fadiga física. O paciente tem dificuldade para relaxar e, muitas vezes, já acorda se sentindo cansado. Como não consegue descansar plenamente na hora de dormir, a pessoa sente o corpo e a mente exaustos praticamente o tempo todo. Também é comum perceber de repente que, sem querer e sem justificativa, estava com os músculos tensionados.
                    <br><br><br>

                    3. PREOCUPAÇÃO EXCESSIVA<br><br>
                    Uma das características mais presentes no transtorno de ansiedade é a preocupação excessiva. É claro que todas as pessoas têm suas preocupações na vida e isso é normal, mas se as suas preocupações são parte dominante dos seus pensamentos na maior parte do dia, de forma que atrapalhem outras tarefas que precisa fazer, isso é um problema.
                    <br><br><br>


                    4. DIFICULDADE PARA SE CONCENTRAR<br><br>
                    Você acorda, se arruma e vai para o trabalho. Faz uma lista de tarefas de tudo o que precisa ser feito naquele dia, mas não consegue focar em nenhuma atividade. Qualquer coisa se torna uma distração e você acaba terminando o dia com um monte de coisas inacabadas. Essa alta dificuldade para se concentrar também pode ser reflexo de ansiedade.
                    <br><br><br>


                    5. IRRITAÇÃO FREQUENTE<br><br>
                    Quando uma pessoa sofre de ansiedade, é comum que ela se estresse de maneira desproporcional, ou seja, pequenos problemas do dia a dia a deixam extremamente irritada. Não só isso, a irritação com algo de pouca importância vai muito além daquele momento; dura às vezes o dia inteiro e ainda torna difícil dormir.
                    <br><br><br>


                    FIQUE ATENTO E BUSQUE TRATAMENTO<br><br>
                    O transtorno de ansiedade generalizada pode se manifestar de maneiras diferentes em cada pessoa. Além dos sintomas mencionados acima, existem outros que podem ocorrer, como falta de ar, palpitações, coração acelerado, náuseas, suor excessivo, dor de cabeça e alteração dos hábitos intestinais.
                    <br><br><br>
                    Caso você se encaixe no que descrevemos, não se acostume ou ache que vai passar sozinho. Procure ajuda profissional, pois somente um médico especializado pode diagnosticar a doença. Lembre-se que ansiedade tem tratamento, que pode incluir terapia e medicamentos.
                </p>

                <!-- TOPICOS / TAGS -->
                <div class="topicos">
                    <div>
                        <p>Tópicos</p>
                    </div>
                    <div>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                        <a href="#">ansiedade</a>
                    </div>
                </div>
            </div>

            <!-- RIGHT WRAPPER -->
            <div class="right-wrapper">
                <div class="destaque-lateral">
                    <h3 class="titulo">Relacionados</h3>
                    <div class="itens-p">
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                    </div>
                </div>

                <div class="destaque-lateral">
                    <h3 class="titulo">Populares</h3>
                    <div class="itens-p">
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                        <a href="#">
                            <article class="item">
                                <figure>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                                </figure>
                                <div class="info">
                                    <h5 class="titulo">Saúde intima</h5>
                                    <p class="texto">Como prevenir a vaginose bacteriana</p>
                                </div>
                            </article>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    

    
<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>