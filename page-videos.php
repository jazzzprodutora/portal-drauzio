<?php // Template Name: Videos ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-videos">

        <!-- UOL WRAPPER -->
        <?php require 'templates/uol-wrapper.php' ?>

        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- NAVEGACAO -->
        <?php require 'templates/navegacao.php' ?>

        <!-- VIDEOS 3 WRAPPER -->
        <div class="videos3-wrapper">
            <div class="header">
                <h3 class="titulo">Vídeos</h3>
            </div>
            <!-- TOPICOS / TAGS -->
            <div class="topicos">
                <div>
                    <p>Séries</p>
                </div>
                <div>
                    <a href="#">Ao Vivão</a>
                    <a href="#">Animações</a>
                    <a href="#">Cabine</a>
                    <a href="#">Colab</a>
                    <a href="#">Coronavírus</a>
                    <a href="#">Dicas de Saúde</a>
                    <a href="#">Dicas Rápidas</a>
                    <a href="#">Drauzio Comenta</a>
                    <a href="#">Drauzio Entrevista</a>
                    <a href="#">Drauzio News</a>
                    <a href="#">Draw</a>
                    <a href="#">Em X Perguntas</a>
                    <a href="#">Entrementes</a>
                    <a href="#">Entrevista em video</a>
                    <a href="#">Especiais</a>
                    <a href="#">Palestras</a>
                    <a href="#">Recorte</a>
                    <a href="#">Séries e Documentários</a>
                    <a href="#">Tira dúvidas com especialista</a>
                </div>
            </div>
            <!-- CAROUSEL -->
            <div id="carousel-videos3" class="splide">
                <div class="splide__track">
                    <div class="splide__list itens">
                        <div class="splide__slide card item">
                            <figure class="imagem">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video3.jpg">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- TYPE 3 WRAPPER -->
        <div class="type3-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img5.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Coluna</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img6.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Entrementes</p>
                            <h2 class="titulo">Você está melhor ou pior que no começo da pandemia?</h2>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- TYPE 5 WRAPPER -->
        <div class="type5-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- TYPE 7 WRAPPER -->
        <div class="type7-wrapper">
            <div class="header">
                <h3 class="titulo">Vídeos Novos</h3>
                <select name="" id="" class="form-select">
                    <option selected>Categorias</option>
                    <option value="Categoria 1">Categoria 1</option>
                    <option value="Categoria 2">Categoria 2</option>
                    <option value="Categoria 3">Categoria 3</option>
                </select>
            </div>
        </div>

        <!-- TYPE 5 WRAPPER -->
        <div class="type5-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="area-botao">
                <a href="#" class="botao botao-principal">Ver Mais</a>
            </div>
        </div>

    </div>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>