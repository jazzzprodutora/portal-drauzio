<?php // Template Name: Categoria ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-categoria">

        <!-- UOL WRAPPER -->
        <?php require 'templates/uol-wrapper.php' ?>

        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- NAVEGACAO -->
        <?php require 'templates/navegacao.php' ?>

        <!-- TYPE 3 WRAPPER -->
        <div class="type3-wrapper">
            <div class="header">
                <h3 class="titulo">Sexualidade</h3>
            </div>
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- TYPE 2 WRAPPER -->
        <div class="type2-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img1.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
            <div class="destaque-lateral">
                <h3 class="titulo">Em Alta</h3>
                <div class="itens-p">
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                </div>
            </div>
        </div>

        <!-- TYPE 6 WRAPPER -->
        <div class="type6-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img7.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img7.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img7.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img7.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- TYPE 3a WRAPPER -->
        <div class="type3a-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img1.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- TYPE 11 WRAPPER -->
        <div class="type11-wrapper">
            <div class="itens">
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#">
                            <p class="area">Psiquiatria</p>
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </a>
                    </div>
                </article>
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#">
                            <p class="area">Psiquiatria</p>
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </a>
                    </div>
                </article>
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#">
                            <p class="area">Psiquiatria</p>
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </a>
                    </div>
                </article>
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#">
                            <p class="area">Psiquiatria</p>
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </a>
                    </div>
                </article>
            </div>
        </div>

        <!-- TYPE 3a WRAPPER -->
        <div class="type3a-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img1.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
        </div>

    </div>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>