<?php // Template Name: Home ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-home">
        <!-- UOL WRAPPER -->
        <?php require 'templates/uol-wrapper.php' ?>

        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- NAVEGACAO -->
        <?php require 'templates/navegacao.php' ?>

        <!-- TYPE 1 WRAPPER -->
        <div class="type1-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- TYPE 2 WRAPPER -->
        <div class="type2-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img1.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
            <div class="destaque-lateral">
                <h3 class="titulo">Em Alta</h3>
                <div class="itens-p">
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                    <a href="#">
                        <article class="item">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/destaque.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <h5 class="titulo">Saúde intima</h5>
                                <p class="texto">Como prevenir a vaginose bacteriana</p>
                            </div>
                        </article>
                    </a>
                </div>
            </div>
        </div>

        <!-- TYPE 10 WRAPPER -->
        <div class="type10-wrapper">
            <div class="header">
                <h3 class="titulo">Podcasts</h3>
                <div class="ver-todos">
                    <a href="#">
                        <span>ver todos</span>
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png" alt="Ver todos" title="">
                        </figure>
                    </a>
                </div>
            </div>
            <!-- CAROUSEL -->
            <div id="carousel-type10" class="splide">
                <div class="splide__track">
                    <div class="splide__list itens">
                        <div class="splide__slide card item">
                            <a href="#">
                                <figure class="imagem">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/podcast.jpg" title="">
                                </figure>
                                <div class="card__info info">
                                    <p class="texto">DrauzioCast #85 | Enxaqueca I</p>
                                </div>
                            </a>
                        </div>
                        <div class="splide__slide card item">
                            <a href="#">
                                <figure class="imagem">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/podcast.jpg" title="">
                                </figure>
                                <div class="card__info info">
                                    <p class="texto">DrauzioCast #85 | Enxaqueca I</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- TYPE 6 WRAPPER -->
        <div class="type6-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                        </div>
                    </a>
                </article>
            </div>
        </div>

        <!-- VIDEOS 2 WRAPPER -->
        <div class="videos2-wrapper">
            <div class="header">
                <h3 class="titulo">Vídeos</h3>
                <div class="ver-todos">
                    <a href="#">
                        <span>ver todos</span>
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png" alt="Ver todos" title="">
                        </figure>
                    </a>
                </div>
            </div>
            <div class="itens">
                <div class="wrapper">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video1.jpg" alt="" title="">
                        </figure>
                        <div class="filtro"></div>
                        <div class="new-wrapper">
                            <div class="body">
                                <p class="texto">Dengue grave | Tira-dúvidas com especialista #12</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="outros-itens">
                    <article class="item">
                        <a href="#">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                            </div>
                        </a>
                    </article>
                    <article class="item">
                        <a href="#">
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/video2.jpg" alt="" title="">
                            </figure>
                            <div class="info">
                                <p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p>
                            </div>
                        </a>
                    </article>
                </div>
            </div>
        </div>

        <!-- TYPE 11 WRAPPER -->
        <div class="type11-wrapper">
            <div class="itens">
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#"><p class="area">Psiquiatria</p></a>
                        <a href="#"><p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p></a>
                    </div>
                </article>
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#"><p class="area">Psiquiatria</p></a>
                        <a href="#"><p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p></a>
                    </div>
                </article>
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#"><p class="area">Psiquiatria</p></a>
                        <a href="#"><p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p></a>
                    </div>
                </article>
                <article class="item">
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img3.jpg" alt="" title="">
                    </figure>
                    <div class="info">
                        <a href="#"><p class="area">Psiquiatria</p></a>
                        <a href="#"><p class="texto">Autismo e seus primeiros sinais | Nuno Lobo Antunes</p></a>
                    </div>
                </article>
            </div>
        </div>

        <!-- TYPE 9 WRAPPER -->
        <div class="type9-wrapper">
            <div class="header">
                <h3 class="titulo">Destaques</h3>
            </div>
            <!-- CAROUSEL -->
            <div id="carousel-type9" class="splide">
                <div class="splide__track">
                    <div class="splide__list itens">
                        <div class="splide__slide card item">
                            <a href="#">
                                <figure class="imagem">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/destaques.jpg" title="">
                                </figure>
                                <div class="card__info info">
                                    <h5 class="nome">Nuno Lobo Antunes 1</h5>
                                    <p class="texto">Autismo e seus primeiros sinais</p>
                                </div>
                            </a>
                        </div>
                        <div class="splide__slide card item">
                            <a href="#">
                                <figure class="imagem">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/destaques.jpg" title="">
                                </figure>
                                <div class="card__info info">
                                    <h5 class="nome">Nuno Lobo Antunes 1</h5>
                                    <p class="texto">Autismo e seus primeiros sinais</p>
                                </div>
                            </a>
                        </div>
                        <div class="splide__slide card item">
                            <a href="#">
                                <figure class="imagem">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/destaques.jpg" title="">
                                </figure>
                                <div class="card__info info">
                                    <h5 class="nome">Nuno Lobo Antunes 1</h5>
                                    <p class="texto">Autismo e seus primeiros sinais</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- TYPE 3 WRAPPER -->
        <div class="type3-wrapper">
            <div class="itens">
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img2.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/img4.jpg" alt="" title="">
                        </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
                <article class="item">
                    <a href="#">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/noticia2.jpg" alt="" title="">
                         </figure>
                        <div class="info">
                            <p class="area">Psiquiatria</p>
                            <h2 class="titulo">5 sinais de que sua ansiedade pode estar passando do ponto</h2>
                        </div>
                    </a>
                </article>
            </div>
        </div>
    </div>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>