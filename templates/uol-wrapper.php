<div class="uol-wrapper">
    <div class="links">
        <a href="#" class="link">Uol Host</a>
        <a href="#" class="link">Pagbank</a>
        <a href="#" class="link">Pagseguro</a>
        <a href="#" class="link">Cursos</a>
    </div>
    <div class="logo-uol">
        <a href="#">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/logo-uol.png" alt="Uol">
        </a>
    </div>
    <div class="actions">
        <a href="#" class="busca-uol">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/busca-uol.png" alt="Busca">
            Busca</a>
        <a href="#" class="bate-papo-uol">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/batepapo-uol.png" alt="Bate-papo">
            Bate-papo</a>
        <a href="mailto:" class="email-uol">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/email-uol.png" alt="Email">
            Email</a>
    </div>
</div>