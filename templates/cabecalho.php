<header class="cabecalho">
    <div class="logo">
        <div class="menu-compacto">
            <div class="linha1"></div>
            <div class="linha2"></div>
            <div class="linha3"></div>
            <span>Menu</span>
        </div>
        <a href="home">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/logo-drauzio.png" alt="Drauzio">
        </a>
    </div>
    <div class="content-wrapper">
        <div class="busca-wrapper" id="area-clique-busca">
            <button class="btn-busca">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/busca.png">
                <span>Pesquisar</span>
            </button>
        </div>
        <div class="redes-sociais">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram.png" alt="Instagram">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.png" alt="Facebook">
            </a>
        </div>
    </div>
</header>

<div class="busca-aberto">
    <div class="fechar-busca">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/fechar.png">
    </div>
    <div class="busca-wrapper">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/busca.png">
        <input type="text" placeholder="Digite o que você está procurando">
    </div>
    <div class="sugestoes">
        <div class="item">
            <div class="nome">Vaginose bacteriana</div>
            <div class="area">Doenças e sintomas</div>
        </div>
        <div class="item">
            <div class="nome">Vaginose bacteriana</div>
            <div class="area">Doenças e sintomas</div>
        </div>
        <div class="item">
            <div class="nome">Vaginose bacteriana</div>
            <div class="area">Doenças e sintomas</div>
        </div>
    </div>
</div>

<div class="menu-aberto">
    <div class="itens">
        <div class="links">
            <div class="link-wrapper">
                <a href="#" class="link-principal">Coronavírus<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Drauzio<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="#" class="link">Biografia</a>
                    <a href="#" class="link">Livros</a>
                    <a href="#" class="link">Artigos</a>
                </div>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Doenças e sintomas<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Ambulatório<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="#" class="link">Febre</a>
                    <a href="#" class="link">Dor de cabeça</a>
                    <a href="#" class="link">Dor de barriga</a>
                    <a href="#" class="link">Dor de garganta</a>
                    <a href="#" class="link">Infecção Urinária</a>
                    <a href="#" class="link">Infarto</a>
                    <a href="#" class="link">AVC</a>
                    <a href="#" class="link">Exames</a>
                </div>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Sexualidade<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="#" class="link">Contracepção</a>
                    <a href="#" class="link">IST</a>
                    <a href="#" class="link">LGBTQI+</a>
                </div>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Criança<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
            </div>
        </div>

        <div class="links">
            <div class="link-wrapper">
                <a href="#" class="link-principal">Mulher<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="#" class="link">Menstruação</a>
                    <a href="#" class="link">Contracepção</a>
                    <a href="#" class="link">Gravidez e amamentação</a>
                    <a href="#" class="link">Menopausa</a>
                </div>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Idoso<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="#" class="link">Reumatismos</a>
                    <a href="#" class="link">Osteoporose</a>
                    <a href="#" class="link">Alzheimer</a>
                </div>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Checagens<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Podcasts<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="#" class="link">DrauzioCast</a>
                    <a href="#" class="link">EntreMentes</a>
                    <a href="#" class="link">Por Que Dói?+</a>
                    <a href="#" class="link">Saúde sem Tabu</a>
                </div>
            </div>
            <div class="link-wrapper">
                <a href="#" class="link-principal">Vídeos<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/seta.png"></a>
                <div class="link-p-wrapper">
                    <a href="videos" class="link">Vídeos</a>
                    <a href="videos-interna" class="link">Vídeos Interna</a>
                    <a href="categoria" class="link">Categoria</a>
                    <a href="doencas-sintomas" class="link">Doenças e Sintomas</a>
                    <a href="interna-post" class="link">Interna Post</a>
                    <a href="colunistas" class="link">Colunistas</a>
                    <!-- <a href="#" class="link">Ao Vivão</a>
                    <a href="#" class="link">Animações</a>
                    <a href="#" class="link">Cabine</a>
                    <a href="#" class="link">Colab</a>
                    <a href="#" class="link">Coronavírus</a>
                    <a href="#" class="link">Dicas de Saúde</a> -->
                    <a href="#" class="link">Dicas Rápidas</a>
                    <a href="#" class="link">Drauzio Comenta</a>
                    <a href="#" class="link">Drauzio Entrevista</a>
                    <a href="#" class="link">Drauzio News</a>
                    <a href="#" class="link">Draw</a>
                </div>
            </div>
        </div>
    </div>
</div>